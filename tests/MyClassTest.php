<?php
namespace MyApp\Tests;

use PHPUnit\Framework\TestCase;
use MyApp\IHttpClient;
use MyApp\MyClass;

class HttpClientMock implements IHttpClient
{
    /** @var array $expectedResponse */
    private $expectedResponse = [];

    /**
     * Constructor
     *
     * @param array $expectedResponse
     */
    public function __construct(array $expectedResponse)
    { 
        $this->expectedResponse = $expectedResponse;
    }

    public function get(string $endpoint): array
    {
        return $this->expectedResponse;
    }
}

class MyClassTest extends TestCase
{
    private $myClass;

    public function setUp(): void
    {
        $httpClientMock = new HttpClientMock(
            $this->getExpectedResponse()
        );

        $this->myClass = new MyClass($httpClientMock);
    }

    public function testHello(): void
    {
        $expectedResponse = $this->getExpectedResponse();
        $result = $this->myClass->hello();
        $this->assertEquals($result, "Hello " . $expectedResponse["name"]);
    }

    private function getExpectedResponse(): array 
    {
        return [
            "name" => "Harun"
        ];
    }
}