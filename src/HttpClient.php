<?php
namespace MyApp;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;

class HttpClient implements IHttpClient
{
    /** @var ClientInterface $client */
    private $client;
    
    /**
     * Constructor
     *
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->client = new Client($config);
    }

    /**
     * Get http response and decode it to mixed array
     *
     * @param string $endpoint
     * @return string
     */
    public function get(string $endpoint): array 
    {
        $response = $this->client->request('GET', $endpoint);
        return json_decode($response->getBody(), true);
    }
}