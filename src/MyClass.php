<?php
namespace MyApp;

class MyClass
{
    /** @var IHttpClient $client */
    private $client;

    /**
     * Constructor
     *
     * @param IHttpClient $client
     */
    public function __construct(IHttpClient $client) 
    {
        $this->client = $client;
    }

    public function hello(): string 
    {
        $user = $this->client->get("users/1");
        $name = array_key_exists("name", $user) ? $user["name"] : "Anonymous";
        return "Hello " . $name;
    }
}