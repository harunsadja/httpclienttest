<?php
namespace MyApp;

interface IHttpClient{
    public function get(string $endpoint): array;
}